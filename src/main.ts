import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ErrorInterceptor } from './interceptors/error.interceptor';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useGlobalInterceptors(new ErrorInterceptor());

  const options  = new DocumentBuilder()
  .setTitle('BadeSaba')
  .setDescription("BadeSaba API Application")
  .setVersion('v1')
  .addBearerAuth(
    { type: 'http', scheme: 'bearer', bearerFormat: 'JWT', in: 'header',
  },
    'JWT-auth',
  ).addSecurity('ApiKeyAuth', {
    type: 'apiKey',
    in: 'header',
    name: 'Authorization',
})
.addSecurityRequirements('ApiKeyAuth')
  .build();

const document = SwaggerModule.createDocument(app, options );
SwaggerModule.setup('api', app, document);


  await app.listen(3000);
}
bootstrap();

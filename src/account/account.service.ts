import { Injectable } from '@nestjs/common';
import { ClientProxy, ClientProxyFactory, Transport } from '@nestjs/microservices';

@Injectable()
export class AccountService {
    private clientProxy : ClientProxy;
    constructor(){
        this.clientProxy = ClientProxyFactory.create({
            transport : Transport.NATS,
            options:{
              urls : ['nats://localhost:4222'],
              queue : 'account_service_queue'
            }
        });
    }


    public hello (){
        console.log('123')
        return this.clientProxy.send<any,any>('msg','thats ok');
    }


    public register(inp){
        return this.clientProxy.send<any,any>('register',inp);
    }
    public logIn(inp){
        return this.clientProxy.send<any,any>('login',inp);
    }

    public me(inp){
        return this.clientProxy.send<any,any>('me',inp);
    }
}

import { Module } from '@nestjs/common';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { AuthGuard } from 'src/guards/auth.guard';
import { RolesGuard } from 'src/guards/roles.guard';
import { AccountController } from './account.controller';
import { AccountService } from './account.service';

@Module({
  imports:[
    ClientsModule.register([
      {
        name : 'ACCOUNT_SERVICE',
        transport : Transport.NATS,
        options:{
          urls : ['nats://localhost:4222'],
          queue : 'account_service_queue'
        }
      }
    ])
  ],
  controllers: [AccountController],
  providers: [AccountService]
})
export class AccountModule {}

import { Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { get } from 'http';
import { AuthGuard } from 'src/guards/auth.guard';
import { AccountService } from './account.service';


@ApiTags('Account')
@Controller('account')
export class AccountController {

constructor(private accountService : AccountService){}

@Get('hello')
getHello(): any{
    return this.accountService.hello();
}


@Post('signup')
async register(@Request() req){
    return this.accountService.register({body : req.body});
}

@Post('login')
async logIn(@Request() req){
    return this.accountService.logIn({body : req.body});
}

@UseGuards(AuthGuard)
@Get('me')
async me(@Request() req){

    return this.accountService.me({headers :  req.headers});
}

}

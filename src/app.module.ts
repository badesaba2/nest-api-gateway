import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AccountModule } from './account/account.module';
import { AuthGuard } from './guards/auth.guard';
import { RolesGuard } from './guards/roles.guard';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ErrorInterceptor } from './interceptors/error.interceptor';

@Module({
  imports: [
    ClientsModule.register([
      {
        name : 'ACCOUNT_SERVICE',
        transport : Transport.NATS,
        options:{
          urls : ['nats://localhost:4222'],
          queue : 'account_service_queue'
        }
      }
    ]),
    AccountModule],
  controllers: [AppController],
  providers: [AppService,{
    provide : 'APP_GUARD',
    useClass : RolesGuard
  },{
    provide : 'APP_INTERCEPTOR',
    useClass : ErrorInterceptor
  }],
})
export class AppModule {}

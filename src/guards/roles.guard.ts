import { CanActivate,ExecutionContext,Logger,Inject } from "@nestjs/common";
import { Reflector } from "@nestjs/core";
import { ClientProxy } from "@nestjs/microservices";
import { Observable } from "rxjs";
import { timeout } from "rxjs/operators";

export class RolesGuard implements CanActivate {
    constructor(@Inject('ACCOUNT_SERVICE') private readonly client : ClientProxy,
    private readonly reflector : Reflector
    ){}


    async canActivate(context: ExecutionContext): Promise<boolean> {

        const req = context.switchToHttp().getRequest();

        try{
        const roles = 
                    this.reflector.get<string[]>('roles',context.getHandler());
        if(!roles || !roles.length){
            return  true;
        }            

        const user = await this.client.send<any>(
            'me',
            {
                headers: req.headers
            }).pipe(timeout(5000)).toPromise();

            const userRoles = user.roles;

            return 
                userRoles.includes('any') ||
                userRoles.some(role =>roles.includes(role))
    }
    catch (err){
        Logger.error(err);
        return false;
    }

    }

}
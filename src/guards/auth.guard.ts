import { CanActivate,ExecutionContext,Logger,Inject } from "@nestjs/common";
import { ClientProxy } from "@nestjs/microservices";
import { Observable } from "rxjs";
import { timeout } from "rxjs/operators";

export class AuthGuard implements CanActivate {
    constructor(@Inject('ACCOUNT_SERVICE') private readonly client : ClientProxy){}


    async canActivate(context: ExecutionContext): Promise<boolean> {

    const req = context.switchToHttp().getRequest();

    try{
        const isAuthed = await this.client.send<boolean>(
            'isAuthed',  
            {
                jwt: req.headers.authorization?.split(' ')[1]
            }).pipe(timeout(5000)).toPromise();

            if(isAuthed && isAuthed!==null){
                req.user=isAuthed;
                return true;

            }else return false;
    }
    catch (err){
        Logger.error(err);
        return false;
    }

    }

}